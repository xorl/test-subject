#!/bin/bash -xa

PATH=$1

#Go to path sepcified
cd $PATH;

# File Generation
for i in `/usr/bin/seq 10`;
do
  echo "foo" >> foo-$i;
done

# the hard way.
for f in *;
do
  if [[ $f =~ 'foo' ]]
  then
    NEW_FILE=${f//foo/bar}
    /bin/mv $f $NEW_FILE;
    if [ -f $NEW_FILE ]
    then
      echo "File $f was renamed to $NEW_FILE.";
      /usr/bin/sed -i '' 's/foo/bar/' $NEW_FILE
    fi
  fi
done
