class agentInit {
    if ($timezone == "UTC") {
        file { "/etc/init.d/agent.init":
            source => "puppet://server/modules/test",
            owner => "root",
            group => "root",
            mode  => 755,
            notify => Service[agent]
        }
        file { "/usr/local/bin/agent":
            source => "puppet://server/modules/test",
            owner => "root",
            group => "root",
            mode  => 755
        }
        service { agent:
            ensure => running,
        }
    }
}

class packages {
    package { mysql-server: ensure => latest }
    package { mysql-client: ensure => latest }
    package { openjdk-6-jre: ensure => latest }
    package { sun-java6-jre: ensure => absent }

    if ($operatingsystemrelease == "10.04") {
        package { firefox: ensure => "10.0.2+build1-0ubuntu0.10.04.1" }
    } elsif ($operatingsystemrelease == "10.10"){
        package { firefox: ensure => "10.0.2+build1-0ubuntu0.10.10.1" }
    }
    file { "/etc/apt/preferences": 
        content => template("apt_preferences.erb"), 
    }
}
