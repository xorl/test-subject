node default {
    include common
    include agentInit
}

node /special\d+\.thousand...\.com$/ {
    include special
    include packages
    include agentInit
    File { backup => 'test', }
}
